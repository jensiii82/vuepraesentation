function addTodo() {
    let newTodoInput = document.getElementById('newTodoInput'); // Textfeld selektieren
    let text = newTodoInput.value //Text selektieren
    let li = document.createElement('li'); // neues list item erstellen
    li.innerText = text; // text ins list item schreiben
    document.getElementById('list').appendChild(li); //list item zur Tabelle hinzufügen
}